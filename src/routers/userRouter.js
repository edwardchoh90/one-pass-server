const express = require('express');
const router = new express.Router();
const User = require('../models/userModel');
const _ = require('lodash');

router.post('/users', async (req, res) => {
  const user = new User(req.body);
  console.log('REQUEST BODY', req.body);
  try {
    const resp = await user.save();
    res.send({
      name: resp.name,
      _id: resp._id,
    });
  } catch (err) {
    res.status(400).send(err);
  }
});

router.post('/users/bulk', async (req, res) => {
  try {
    let userlist = _.values(req.body);

    for (let i = 0; i < userlist.length; i++) {
      if (userlist[i].name) {
        console.log('userl', userlist[i]);
        const user = new User({
          name: userlist[i].name,
          employeeID: userlist[i].id,
          fingerprintData: userlist[i].fingerprintData,
          _id: userlist[i].nodeID,
        });
        const resp = await user.save();
      }
    }
  } catch (err) {
    console.log(err);
  } finally {
    res.send('200');
  }
});

router.get('/users', async (req, res) => {
  try {
    let users = await User.find({});
    res.send(users);
  } catch (err) {
    res.status(400).send(err);
  }
});

router.get('/userfp', async (req, res) => {
  try {
    let users = await User.find({ fingerprintData: { $exists: true } });
    res.send(users);
  } catch (err) {
    console.log('ERROR IN GETFP', err);
    res.status(400).send(err);
  }
});

router.put('/users/:id', async (req, res) => {
  const _id = req.params.id;
  try {
    let user = await User.findByIdAndUpdate(_id, req.body, {
      new: true,
    });
    if (!user) {
      return res.status(404).send();
    }
    res.send(user);
  } catch (e) {
    res.status(400).send(e);
  }
});

router.delete('/users/:id', async (req, res) => {
  try {
    const user = await User.findByIdAndDelete(req.params.id);
    if (!user) {
      return res.status(404).send();
    }
    res.send(user);
  } catch (e) {
    res.status(500).send(e);
  }
});

module.exports = router;
