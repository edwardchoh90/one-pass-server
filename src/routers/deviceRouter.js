const express = require('express')
const router = new express.Router();
const Device = require("../models/deviceModel");


router.post('/newdevice', async (req, res) => {
    const device = new Device(req.body)
    try {
        await device.save()
        res.status(201).send("success")
    } catch (err) {
        console.log("ERROR", err)
        res.status(401).send(err)
    }
})


router.get("/getdevices", async (req, res) => {
    try {
        let devices = await Device.find({});
        res.send(devices);
    } catch (err) {
        res.status(400).send(err);
    }
});

router.delete("/removedevice/:id", async (req, res) => {
    try {
        const device = await Device.findByIdAndDelete(req.params.id);
        if (!device) {
            return res.status(404).send();
        }
        res.send("Delete Successfully");
    } catch (e) {
        res.status(500).send(e);
    }
});

router.put("/editdevice/:id", async (req, res) => {
    console.log("REQUEST", req)
    const _id = req.params.id;
    try {
        let device = await Device.findByIdAndUpdate(_id, req.body, {
            new: true
        })
        if (!device) {
            return res.status(404).send();
        }
        res.send(device);
    } catch (e) {
        res.status(400).send(e);
    }
})


module.exports = router