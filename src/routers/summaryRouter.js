const express = require("express");
const router = new express.Router();
const Attendance = require("../models/attendanceModel");
const Summary = require("../models/summaryModel");
const User = require("../models/userModel");

router.get("/summary", async (req, res) => {
  try {
    let searchCriteria = {
      clockDate: req.query.date
    };

    let userQuery = User.find({}, { shift: 1, employeeID: 1, name: 1 }).exec();
    userQuery.then(data => {
      let attendanceEnquiry = [];
      data.forEach(res => {
        attendanceEnquiry.push(
          Attendance.find(
            {
              employeeID: res.employeeID,
              clockDate: searchCriteria.clockDate
            },
            { clockTime: 1, activity_type: 1, clockDate: 1, _id: 0 }
          ).exec()
        );
      });

      Promise.all(attendanceEnquiry).then(result => {
        res.send({ users: data, attendance: result });
      });
    });
  } catch (err) {
    res.status(400).send(err);
  }
});

module.exports = router;
