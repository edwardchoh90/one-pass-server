const express = require('express');
const router = new express.Router();
const Attendance = require('../models/attendanceModel');
const User = require('../models/userModel');

router.post('/attendance', async (req, res) => {
  let userData = null;
  try {
    userData = await User.findOne(
      {
        employeeID: req.body.employeeID,
      },
      { shift: 1, employeeID: 1, name: 1 }
    );
  } catch (e) {
    userData = null;
  }

  const attendance = new Attendance({
    ...req.body,
    user: userData,
  });

  try {
    const resp = await attendance.save();
    res.send('attendance registered');
    global.io.emit('newAttendance', req.body);
  } catch (err) {
    res.status(400).send(err);
  }
});

router.get('/attendance', async (req, res) => {
  let searchCriteria = {
    clockDate: req.query.date,
  };

  try {
    let attendance = await Attendance.find(searchCriteria);

    res.send(attendance);
  } catch (err) {
    res.status(400).send(err);
  }
});

module.exports = router;
