const express = require("express");
const router = new express.Router();
const Schedule = require("../models/scheduleModel");

router.post("/schedule", async (req, res) => {
  const schedule = new Schedule(req.body);
  try {
    await schedule.save();
    res.status(201).send("success");
  } catch (err) {
    console.log("ERROR", err);
    res.status(401).send(err);
  }
});

router.get("/schedule", async (req, res) => {
  try {
    let schedule = await Schedule.find({});
    res.send(schedule);
  } catch (err) {
    res.status(400).send(err);
  }
});

/* router.put("/holiday/:id", async (req, res) => {
  console.log("REQUEST", req);
  const _id = req.params.id;
  try {
    let holiday = await Holiday.findByIdAndUpdate(_id, req.body, {
      new: true
    });
    console.log("HOLZ", holiday);
    if (!holiday) {
      return res.status(404).send();
    }
    res.send(holiday);
  } catch (e) {
    res.status(400).send(e);
  }
}); */

module.exports = router;
