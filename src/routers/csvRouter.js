const express = require("express");
const router = new express.Router();
const Attendance = require("../models/attendanceModel");
const User = require("../models/userModel");
const { parse } = require("json2csv");

router.get("/usercsv", async (req, res) => {
  try {
    let user = await User.find({});
    const fields = [
      "name",
      "employeeID",
      "organization",
      "placholder",
      "job_title",
      "joinedDate"
    ];
    const opts = { fields };

    const csv = parse(user, opts);
    res.attachment("data.csv");
    res.send(csv);
  } catch (err) {
    console.log("ERR", err);
    res.status(400).send(err);
  }
});

router.get("/attendancecsv", async (req, res) => {
  try {
    let attendance = await Attendance.find({});
    const fields = [
      "name",
      "employeeID",
      "deviceID",
      "clockTime",
      "clockDate",
      "lat",
      "lng",
      "activity_type"
    ];
    const opts = { fields };

    const csv = parse(attendance, opts);
    res.attachment("attendance.csv");
    res.send(csv);
  } catch (err) {
    console.log("ERR", err);
    res.status(400).send(err);
  }
});

module.exports = router;
