const express = require('express')
const router = new express.Router();
const Role = require("../models/roleModel");

router.post('/login', async (req, res) => {
    try {
        const role = await Role.findByCredentials(req.body.username, req.body.password)
        res.send('success')
    } catch (e) {
        res.status(401).send()
    }
})

router.post('/createRole', async (req, res) => {
    const role = new Role(req.body)
    try {
        await role.save()
        res.status(201).send("success")
    } catch (err) {
        console.log("ERROR", err)
        res.status(401).send(err)
    }
})

module.exports = router