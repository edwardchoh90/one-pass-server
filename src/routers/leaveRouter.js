const express = require('express')
const router = new express.Router();
const Leave = require("../models/leaveModel");


router.post('/applyLeave', async (req, res) => {
    const leave = new Leave(req.body)
    try {
        await leave.save()
        res.status(201).send("success")
    } catch (err) {
        console.log("ERROR", err)
        res.status(401).send(err)
    }
})


router.get("/getleave", async (req, res) => {
    try {
        let leave = await Leave.find({});
        res.send(leave);
    } catch (err) {
        res.status(400).send(err);
    }
});

router.delete("/removeleave/:id", async (req, res) => {
    try {
        const leave = await Leave.findByIdAndDelete(req.params.id);
        if (!leave) {
            return res.status(404).send();
        }
        res.send("Delete Successfully");
    } catch (e) {
        res.status(500).send(e);
    }
});

router.put("/editleave/:id", async (req, res) => {
    console.log("REQUEST", req)
    const _id = req.params.id;
    try {
        let leave = await Leave.findByIdAndUpdate(_id, req.body, {
            new: true
        })
        if (!leave) {
            return res.status(404).send();
        }
        res.send(leave);
    } catch (e) {
        res.status(400).send(e);
    }
})


module.exports = router