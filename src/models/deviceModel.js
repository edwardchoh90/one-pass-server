const mongoose = require("mongoose");

const DeviceSchema = new mongoose.Schema({

    deviceSerial: {
        type: String,
        default: "N/A"
    },
    deviceModel: {
        type: String,
        default: "N/A",
    },
    userCounts: {
        type: Number,
        default: 0,
    },
    activatedDate: {
        type: String,
        default: "Not Activated",
    },
    connectionStatus: {
        type: Boolean,
        default: false
    },
    terminalID: {
        required: true,
        type: String,
    },
    deviceUser: {
        required: true,
        type: Object
    },
    shiftData: {
        required: true,
        type: Object
    },
    remark: {
        required: true,
        type: String
    },
    status: {
        type: Object,
    },
    locationName: {
        type: String,
        required: true
    },
    locationData: {
        type: Object,
        required: true
    }
})

const Device = mongoose.model("Device", DeviceSchema);


module.exports = Device