const mongoose = require("mongoose");

const HolidaySchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
    },
    description: {
        default: "",
        type: String,
    },
    type: {
        type: Object,
        required: true,
    },
    allDay: {
        type: Boolean,
    },
    startDate: {
        type: String,
        required: true
    },
    startTime: {
        type: String
    },
    endDate: {
        type: String,
        required: true
    },
    endTime: {
        type: String,
    },


})

const Holiday = mongoose.model("Holiday", HolidaySchema);


module.exports = Holiday