const mongoose = require("mongoose");
const userModel = require("./userModel");
const scheduleModel = require("./scheduleModel");

const AttendanceSchema = new mongoose.Schema({
  name: {
    type: String
  },
  employeeID: {
    type: String
  },
  deviceID: {
    type: String
  },
  clockTime: {
    type: String
  },
  clockDate: {
    type: String
  },
  lat: {
    type: Number
  },
  lng: {
    type: Number
  },
  activity_type: {
    type: String
  },
  user: {
    type: Object,
    ref: "User"
  }
});

const Attendance = mongoose.model("Attendance", AttendanceSchema);

module.exports = Attendance;
