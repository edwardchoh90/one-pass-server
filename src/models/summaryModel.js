const mongoose = require("mongoose");
const UserSchema = require("./userModel");
const AttendanceSchema = require("./attendanceModel");
const LeaveSchema = require("./leaveModel");

const summarySchema = new mongoose.Schema({
  user: {
    type: Object
  },
  attendance: {
    type: Object
  },
  leave: {
    type: Object
  }
});

const Summary = mongoose.model("Summary", summarySchema);

module.exports = Summary;
