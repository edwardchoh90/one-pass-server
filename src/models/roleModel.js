const mongoose = require("mongoose");
const bcrypt = require('bcryptjs');

const roleSchema = new mongoose.Schema({
    user: {
        type: Object,
        required: true,
    },
    username: {
        type: String,
        required: true,
        trim: true,
    },
    password: {
        type: String,
        required: true,
        trim: true
    },
    designation: {
        type: Object,
        required: true,
        trim: true
    }
})
//password Hashing before Saving
roleSchema.pre('save', async function (next) {

    const role = this
    if (role.isModified('password')) {
        role.password = await bcrypt.hash(role.password, 8)
    }

    next()
})

roleSchema.statics.findByCredentials = async (username, password) => {
    const role = await Role.findOne({
        username
    })
    if (!role) {
        throw new Error('Invalid login credentials')
    }
    const isMatch = await bcrypt.compare(password, role.password)
    if (!isMatch) {
        throw new Error('Invalid login credentials')
    }
    return role
}

const Role = mongoose.model('Role', roleSchema)
module.exports = Role