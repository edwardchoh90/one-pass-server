const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true,
  },
  employeeID: {
    type: String,
    trim: true,
  },
  organization: {
    type: String,
    trim: true,
    default: '',
  },
  job_title: {
    type: String,
    trim: true,
  },
  designation: {
    type: Object,
  },
  position_level: {
    type: Object,
  },
  last_activiy: {
    type: Object,
    default: {
      activity: '',
    },
  },
  shift: {
    type: Object,
  },
  employmentStatus: {
    type: Object,
  },
  joinedDate: {
    type: String,
  },
  effectiveDate: {
    type: String,
  },
  dateOfBirth: {
    type: String,
  },
  gender: {
    type: String,
  },
  race: {
    type: String,
  },
  religion: {
    type: String,
  },
  gender: {
    type: String,
  },
  address: {
    type: String,
  },
  race: {
    type: String,
  },
  nationality: {
    type: Object,
  },
  remark: {
    type: String,
  },
  fingerprintData: {},
});

const User = mongoose.model('User', UserSchema);

/*
UserSchema.pre('save', async function (next) {

    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)

    }

    next()
})
*/

module.exports = User;
