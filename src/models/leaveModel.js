const mongoose = require("mongoose");

const LeaveSchema = new mongoose.Schema({
    user: {
        type: Object,
        required: true,
    },
    leaveType: {
        type: Object,
        required: true,
    },
    leaveStart: {
        type: String,
        required: true,
    },
    leaveEnd: {
        type: String,
        required: true,
    },
    duration: {
        type: Number,
        required: true
    },
    dayOption: {
        type: Object
    },
    reason: {
        type: String,
    }
})

const Leave = mongoose.model("Leave", LeaveSchema);


module.exports = Leave