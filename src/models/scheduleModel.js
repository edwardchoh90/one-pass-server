const mongoose = require("mongoose");

const ScheduleSchema = new mongoose.Schema({
  offDays: {
    type: Array
  },
  scheduleDescription: {
    type: String
  },
  scheduleName: {
    type: String
  },
  workDays: {
    type: Array
  },
  rules: {
    allowFlexiBreak: {
      type: Boolean
    },
    applyBreaktoAttendance: {
      type: Boolean
    },
    deductBreakFromWorkTime: {
      type: Boolean
    },
    deductBreakMinute: {
      type: Number
    },
    deductExtraBreakfromAttendance: {
      type: Boolean
    },
    flexiBreakMinute: {
      type: Number
    },
    lunchTimeAsWorkHour: {
      type: Boolean
    },
    remainingBreakTimeCount: {
      type: String
    },
    roundClockMinute: {
      type: Number
    },
    roundUpClockTime: {
      type: Boolean
    },
    takeClockTimeAsOut: {
      type: Boolean
    }
  }
});

const Schedule = mongoose.model("Schedule", ScheduleSchema);

/*
UserSchema.pre('save', async function (next) {

    const user = this
    if (user.isModified('password')) {
        user.password = await bcrypt.hash(user.password, 8)

    }

    next()
})
*/

module.exports = Schedule;
