const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const userRouter = require('./routers/userRouter');
const roleRouter = require('./routers/roleRouter');
const holidayRouter = require('./routers/holidayRouter');
const leaveRouter = require('./routers/leaveRouter');
const deviceRouter = require('./routers/deviceRouter');
const attendanceRouter = require('./routers/attendanceRouter');
const scheduleRouter = require('./routers/scheduleRouter');
const summaryRouter = require('./routers/summaryRouter');
const csvRouter = require('./routers/csvRouter');

const app = express(); // create your express app

require('./db/mongoose.js');
require('./db/backup-db.js');

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.use(cors());
app.use(userRouter);
app.use(roleRouter);
app.use(holidayRouter);
app.use(leaveRouter);
app.use(attendanceRouter);
app.use(deviceRouter);
app.use(scheduleRouter);
app.use(summaryRouter);
app.use(csvRouter);
const server = app.listen(process.env.PORT || 3000, () => {
  console.log('MEVN IS LIVE');
});

const io = require('socket.io')(server);
global.io = io;

io.on('connection', function(socket) {
  console.log(socket.id);
});
